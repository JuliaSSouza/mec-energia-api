from utils.user.user_type_util import UserType
from users.models import models, CustomUser, UniversityUser
import pytest
from users import models

#CT1
def test_invalid_user_type():
    invalid_user_type = "invalid_user"

    try:
        UserType.is_valid_user_type(invalid_user_type)
    except Exception as e:
        exception_message = str(e)
        assert f'User type ({invalid_user_type}) does not exist' in exception_message

#CT2
def test_valid_user_type():
    valid_user_type = "super_user"

    try:
        result = UserType.is_valid_user_type(valid_user_type)
        assert result == valid_user_type
    except Exception as e:
        pass

#CT3
def test_invalid_user_type_for_university_user():
    user_model = models.UniversityUser
    invalid_user_type = "invalid_user"

    try:
        UserType.is_valid_user_type(invalid_user_type, user_model)
    except Exception as e:
        exception_message = str(e)
        assert f'User type ({invalid_user_type}) does not exist' in exception_message

#CT4
def test_invalid_user_type_with_invalid_user_model():
    invalid_type = 'invalid_user_type'
    invalid_user_model = models.CustomUser  # Supondo um modelo de usuário válido que não seja UniversityUser

    try:
        result = UserType.is_valid_user_type(valid_user_type, valid_user_model)
        assert result == valid_user_type
    except Exception as e:
        assert False, f"Unexpected exception: {e}"
#CT5

def test_invalid_user_type_with_valid_user_model():
    valid_user_type = models.CustomUser.university_user_type
    valid_user_model = models.CustomUser  # Supondo um modelo de usuário válido que não seja UniversityUser

    try:
        result = UserType.is_valid_user_type(valid_user_type, valid_user_model)
        assert result == valid_user_type
    except Exception as e:
        assert False, f"Unexpected exception: {e}"

#CT6

def test_invalid_user_type_for_custom_user():
    invalid_user_type = "invalid_user"
    user_model = models.CustomUser  # Assuming a valid user model

    try:
        UserType.is_valid_user_type(invalid_user_type, user_model)
    except Exception as e:
        exception_message = str(e)
        assert f'User type ({invalid_user_type}) does not exist' in exception_message
    else:
        assert False, f"An unexpected exception occurred for user type: {invalid_user_type} and user model: {user_model}"

#CT7

def test_valid_super_user_type_for_custom_user():
    valid_user_type = models.CustomUser.super_user_type
    user_model = models.CustomUser  # Assuming a valid user model

    try:
        result = UserType.is_valid_user_type(valid_user_type, user_model)
        assert result == valid_user_type
    except Exception as e:
        assert False, f"An unexpected exception occurred for user type: {valid_user_type} and user model: {user_model}"

#CT8
def test_invalid_user_type_model_for_custom_user():
    invalid_user_type = "invalid_user"
    user_model = models.UniversityUser  # Assuming a valid user model

    try:
        result = UserType.is_valid_user_type(invalid_user_type, user_model)
        assert result == invalid_user_type  # Verify the returned value matches the input
    except Exception as e:
        assert False, f"Unexpected exception occurred: {e}"
